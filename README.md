# gitlab-rhel-docker

This cookbook configures a rhel machine to build docker images for GitLab

```
(
set -e
set -u

# Install the Chef client
curl -L https://www.chef.io/chef/install.sh | sudo bash

# Download the gitlab-omnibus-builder cookbook using Git and run its 'default' recipe
chef_root=/tmp/gitlab-omnibus-builder.$$
mkdir "$chef_root"
cd "$chef_root"
mkdir cookbooks
git clone https://gitlab.com/twk3/gitlab-rhel-docker.git cookbooks/gitlab-rhel-docker
git clone https://gitlab.com/gitlab-cookbooks/gitlab-omnibus-builder.git cookbooks/gitlab-omnibus-builder
git clone https://gitlab.com/gitlab-cookbooks/gitlab-attributes-with-secrets.git cookbooks/gitlab-attributes-with-secrets
git clone https://gitlab.com/gitlab-cookbooks/gitlab-vault.git cookbooks/gitlab-vault
/opt/chef/bin/chef-client -z -r 'recipe[gitlab-rhel-docker::default]'
)
```
