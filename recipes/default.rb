#
# Cookbook Name:: gitlab-rhel-docker
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

execute "enable repos" do
  command "subscription-manager repos --enable=rhel-7-server-rpms --enable=rhel-7-server-extras-rpms --enable=rhel-7-server-optional-rpms"
end

include_recipe 'gitlab-omnibus-builder::git'

include_recipe 'gitlab-omnibus-builder::gitlab-ci-multi-runner'

include_recipe 'gitlab-rhel-docker::docker'

# Install secrets for fetching EE source code, pushing packages
include_recipe 'gitlab-omnibus-builder::secrets'
