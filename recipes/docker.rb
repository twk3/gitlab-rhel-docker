package docker

bash "enable docker service" do
  code <<-EOS
set -e # Exit on failure
systemctl enable docker
systemctl start docker
EOS
end
